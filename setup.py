from setuptools import setup
from setuptools_rust import Binding, RustExtension

setup(
    name='foo',
    version='1.0',
    rust_extensions=[
        RustExtension('libfoo', 'Cargo.toml', binding=Binding.PyO3)
    ],
    setup_requires=[
        'setuptools',
        'setuptools_rust'
    ],
    zip_safe=False
)
