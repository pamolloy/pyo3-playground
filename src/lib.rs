#![feature(extern_prelude, proc_macro, proc_macro_path_invoc, specialization)]

extern crate pyo3;

use pyo3::prelude::*;

mod bar;

#[py::modinit(libfoo)]
fn init_mod(py: Python, m: &PyModule) -> PyResult<()> {
    bar::init_mod(py, &m)
}
