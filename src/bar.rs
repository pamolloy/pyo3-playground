use pyo3::prelude::*;

use pyo3;

#[py::modinit(bar)]
pub fn init_mod(py: Python, m: &PyModule) -> PyResult<()> {
    let bar_mod = PyModule::new(py, "bar")?;

    m.add("bar", bar_mod)?;

    #[pyfn(bar_mod, "sum_as_string")]
    fn sum_as_string_py(a:i64, b:i64) -> PyResult<String> {
       let out = sum_as_string(a, b);
       Ok(out)
    }

    Ok(())
}

fn sum_as_string(a:i64, b:i64) -> String {
    format!("{}", a + b).to_string()
}
